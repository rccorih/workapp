package com.example.marketingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //METODO: BOTON ENVIAR A MENU CUIDADO
    public void menu_cuidado(View view){
        Intent menu_cuidado = new Intent(this, MenuCuidado.class);
        startActivity(menu_cuidado);
    }

    //METODO: BOTON ENVIAR A MENU ALIMENTACION
    public void menu_alimentacion(View view){
        Intent menu_alimentacion = new Intent(this, MenuAlimentacion.class);
        startActivity(menu_alimentacion);
    }

    //METODO: BOTON ENVIAR A MENU CUIDADO
    public void menu_salud(View view){
        Intent menu_salud = new Intent(this, MenuSalud.class);
        startActivity(menu_salud);
    }
}
